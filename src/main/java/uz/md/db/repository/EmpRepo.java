package uz.md.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.md.db.domain.Employee;

import java.util.List;

/**
 * Created by MHusanov on 08.12.2020.
 */
@Repository
public interface EmpRepo extends JpaRepository<Employee,Long>{

    List<Employee> findByName(String name);

    List<Employee> findByNameIsStartingWith(String name);

    List<Employee> findByNameIsStartingWithOrderByIdDesc(String name);

    @Query("select e from Employee e where lower(e.name) like lower(:name) ")
    List<Employee> findByNameQuery(@Param("name") String name);

    @Query(value = "select * from Employee e where lower(e.name) like lower(:name) ",nativeQuery = true)
    List<Employee> findByNameQueryNative(@Param("name") String name);
}
