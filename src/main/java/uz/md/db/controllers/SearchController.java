package uz.md.db.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.md.db.models.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MHusanov on 08.12.2020.
 */
@RestController
@RequestMapping("/api")
public class SearchController {
    @GetMapping("/start")
    public String welcome(){
        return "welcome";
    }

    @GetMapping("/clients/all")
    public ResponseEntity getAllClients(){
        Client client1 = new Client("1","Mhusanov","AA7248011","123123123");
        Client client2 = new Client("2","DHusanova","AA7248011","434234324");
        List<Client> clients = new ArrayList<>();
        clients.add(client1);
        clients.add(client2);
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/client/{id}")
    public ResponseEntity getClient(@PathVariable Long id){
        Client client = new Client("1","Mhusanov","AA7248011","123123123");
        return ResponseEntity.ok(client);
    }

    @PostMapping("/client")
    public ResponseEntity saveClient(@RequestBody Client client){
        return ResponseEntity.ok(client);
    }

    @PutMapping("/client/{id}")
    public ResponseEntity updateClient(@PathVariable Long id,@RequestBody Client client){
        return ResponseEntity.ok(client);
    }

    @GetMapping("/client")
    public ResponseEntity putClient(@RequestParam String id,
                                    @RequestParam String name,
                                    @RequestParam String docNumber,
                                    @RequestParam String inps){
        Client client = new Client(id,name,docNumber,inps);
        return ResponseEntity.ok(client);
    }

    @DeleteMapping("/client/{id}")
    public ResponseEntity removeClient(@PathVariable Long id){
        return ResponseEntity.ok("Deleted");
    }
}
