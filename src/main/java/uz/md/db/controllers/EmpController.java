package uz.md.db.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.md.db.domain.Employee;
import uz.md.db.service.EmpService;

import java.util.List;

/**
 * Created by MHusanov on 08.12.2020.
 */
@RestController
@RequestMapping("/api")
public class EmpController {

    private final EmpService empService;

    public EmpController(EmpService empService) {
        this.empService = empService;
    }

    @GetMapping("/emps/all")
    public ResponseEntity getAllEmps(){
        List<Employee> emps = empService.getAllEmps();
        return ResponseEntity.ok(emps);
    }

    @PostMapping("/emp")
    public ResponseEntity create(@RequestBody Employee emp){
        Employee emp2= empService.save(emp);
        return ResponseEntity.ok(emp2);
    }

    @PutMapping("/emp")
    public ResponseEntity update(@RequestBody Employee emp) {
        if (emp.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        Employee emp2 = empService.save(emp);
        return ResponseEntity.ok(emp2);
    }

    @GetMapping("/emps/{name}")
    public ResponseEntity getEmpsByName(@PathVariable String name){
        List<Employee> emps = empService.getEmpsByName(name);
        return ResponseEntity.ok(emps);
    }

    @DeleteMapping("/emps/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        empService.deleteEmp(id);
        return ResponseEntity.ok("Data deleted");
    }
}
