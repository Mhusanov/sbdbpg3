package uz.md.db.service;

import org.springframework.stereotype.Service;
import uz.md.db.domain.Employee;
import uz.md.db.repository.EmpRepo;

import java.util.List;

/**
 * Created by MHusanov on 08.12.2020.
 */
@Service
public class EmpService {

    private final EmpRepo empRepo;

    public EmpService(EmpRepo empRepo) {
        this.empRepo = empRepo;
    }

    public Employee save(Employee emp){
        return empRepo.save(emp);
    }

    public List<Employee> getAllEmps(){
        return empRepo.findAll();
    }

    public Employee getEmp(Long id){
        return empRepo.findById(id).get();
    }

    public List<Employee> getEmpsByName(String name){
        return empRepo.findByNameQueryNative(name);
    }

    public void deleteEmp(Long id) {
        Employee emp = empRepo.getOne(id);
        empRepo.delete(emp);
    }
}
