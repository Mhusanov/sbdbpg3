package uz.md.db.models;

/**
 * Created by MHusanov on 08.12.2020.
 */
public class Client {

    private String id;
    private String name;
    private String docNumber;
    private String inps;

    public Client(){}

    public Client(String id, String name, String docNumber, String inps) {
        this.id = id;
        this.name = name;
        this.docNumber = docNumber;
        this.inps = inps;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getInps() {
        return inps;
    }

    public void setInps(String inps) {
        this.inps = inps;
    }

}
